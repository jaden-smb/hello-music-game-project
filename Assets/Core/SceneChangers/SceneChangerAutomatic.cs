using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChangerAutomatic : MonoBehaviour
{
    public string scenename;
    public float delay = 5f;

    private void Start()
    {
        Invoke("ChangeScene", delay);
    }

    private void ChangeScene()
    {
        SceneManager.LoadScene(scenename);
    }
}