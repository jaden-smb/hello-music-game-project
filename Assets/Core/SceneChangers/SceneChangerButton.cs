using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneChangerButton: MonoBehaviour
{
    public string scenename;
    public Button startButton;
    public Button startTutorialGameButton;
    public Canvas CanvasMenu;
    public Canvas CanvasTutorial;

    private void Start()
    {
        // Add listeners to the buttons
        startButton.onClick.AddListener(StartButtonClicked);
        startTutorialGameButton.onClick.AddListener(StartTutorialGameButtonClicked);
    }

    private void StartButtonClicked()
    {
        // Hide CanvasMenu and show CanvasTutorial when startButton is clicked
        CanvasMenu.gameObject.SetActive(false);
        CanvasTutorial.gameObject.SetActive(true);
    }

    private void StartTutorialGameButtonClicked()
    {
        // Change the scene when startTutorialGameButton is clicked
        SceneManager.LoadScene(scenename);
    }
}