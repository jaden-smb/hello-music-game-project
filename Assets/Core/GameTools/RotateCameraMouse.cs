using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateCameraMouse : MonoBehaviour
{
    [SerializeField]
    private FixedJoystick joystick;

    [SerializeField]
    private GameObject joystickGameObject;

    [SerializeField]
    private float sensitivity = 0.1f;

    [SerializeField]
    // private float speed = 2.0f; // Speed of camera rotation for PC

    private bool isOnMobile;

    // private void Start()
    // {
    //     // Check if the joystick is assigned
    //     if (joystick == null)
    //     {
    //         // Check if the joystickGameObject is assigned
    //         if (joystickGameObject != null)
    //         {
    //             joystick = joystickGameObject.GetComponent<FixedJoystick>();
    //         }
    //     }

    //     // Check if the joystick is assigned
    //     if (joystick != null)
    //     {
    //         isOnMobile = true;
    //     }
    //     else
    //     {
    //         isOnMobile = false;
    //     }
    // }

    private void FixedUpdate()
    {
        // if (isOnMobile)
        // {
        //     // Mobile platform detected, rotate with joystick
        //     if (joystick.Horizontal != 0 || joystick.Vertical != 0)
        //     {
        //         float x = joystick.Horizontal * sensitivity;
        //         float y = joystick.Vertical * sensitivity;

        //         // Calculate the new rotation angles
        //         float rotationY = transform.localEulerAngles.y + x;
        //         float rotationX = transform.localEulerAngles.x - y;

        //         // Apply the rotation to the transform
        //         transform.localEulerAngles = new Vector3(rotationX, rotationY, 0);
        //     }
        // }
        // else
        // {
        //     // PC platform detected, rotate with WASD
        //     float x = Input.GetAxis("Horizontal") * speed;
        //     float y = Input.GetAxis("Vertical") * speed;

        //     // Calculate the new rotation angles
        //     float rotationY = transform.localEulerAngles.y + x;
        //     float rotationX = transform.localEulerAngles.x - y;

        //     // Apply the rotation to the transform
        //     transform.localEulerAngles = new Vector3(rotationX, rotationY, 0);
        // }
        // Mobile platform detected, rotate with joystick
        if (joystick.Horizontal != 0 || joystick.Vertical != 0)
        {
            float x = joystick.Horizontal * sensitivity;
            float y = joystick.Vertical * sensitivity;

            // Calculate the new rotation angles
            float rotationY = transform.localEulerAngles.y + x;
            float rotationX = transform.localEulerAngles.x - y;

            // Apply the rotation to the transform
            transform.localEulerAngles = new Vector3(rotationX, rotationY, 0);
        }
    }
}
