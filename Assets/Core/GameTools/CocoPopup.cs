using UnityEngine;

public class CocoPopup : MonoBehaviour
{
    // Reference to the PlayPauseButton script
    public PlayPauseButton playPauseButton;

    private void OnEnable()
    {
        // Pause the spline animation when CocoPopup is activated
        playPauseButton.PauseSplineAnimation();
    }

    private void OnDisable()
    {
        // Continue the spline animation when CocoPopup is deactivated
        playPauseButton.PlaySplineAnimation();
    }
}