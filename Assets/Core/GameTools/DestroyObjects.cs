using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class DestroyObjects : MonoBehaviour
{
    private Camera mainCamera;
    private int destroyedObjectsCount = 0;
    public Text destroyedObjectsText;
    private GameObject cocoCanvasObj;
    private GameObject cocoPopupObj;
    private GameObject ValentiaObj;
    private GameObject ValentiaPopupObj;

    void Start()
    {
        mainCamera = FindObjectOfType<Camera>();
        UpdateScoreText();

        // Find and initialize the CocoCanvas and CocoPopup objects
        cocoCanvasObj = GameObject.Find("CocoCanvas");
        cocoPopupObj = GameObject.Find("CocoPopup");
        ValentiaObj = GameObject.Find("ValentiaCanvas");
        ValentiaPopupObj = GameObject.Find("ValentiaPopup");

        // Deactivate the CocoCanvas and CocoPopup objects
        if (cocoCanvasObj != null)
        {
            cocoCanvasObj.SetActive(false);
        }

        if (cocoPopupObj != null)
        {
            cocoPopupObj.SetActive(false);
        }

        if (ValentiaObj != null)
        {
            ValentiaObj.SetActive(false);
        }

        if (ValentiaPopupObj != null)
        {
            ValentiaPopupObj.SetActive(false);
        }
    }

    void Update()
    {
        // Check for mouse click input
        if (Input.GetMouseButtonDown(0))
        {
            // Cast a ray from the mouse position
            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            ProcessRaycast(ray);
        }

        // Check for touch input
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            // Check if the touch just began
            if (touch.phase == TouchPhase.Began)
            {
                // Cast a ray from the touch position
                Ray touchRay = mainCamera.ScreenPointToRay(touch.position);
                ProcessRaycast(touchRay);
            }
        }

        if (SceneManager.GetActiveScene().name == "scene_2" || SceneManager.GetActiveScene().name == "scene_3")
        {
            // Activate the CocoCanvas if it exists
            if (cocoCanvasObj != null)
            {
                cocoCanvasObj.SetActive(true);
            }
        }

        if (SceneManager.GetActiveScene().name == "scene_3")
        {
            // Activate the Valentia object
            if (ValentiaObj != null)
            {
                ValentiaObj.SetActive(true);
            }
        }

        if (SceneManager.GetActiveScene().name == "main_menu")
        {
            // Move the CocoCanvas back to a new scene
            if (cocoCanvasObj != null)
            {
                SceneManager.MoveGameObjectToScene(cocoCanvasObj, SceneManager.GetActiveScene());
            }
        }
    }

    void ProcessRaycast(Ray ray)
    {
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.gameObject.CompareTag("ScoreObject"))
            {
                Destroy(hit.collider.gameObject);
                destroyedObjectsCount++;
                UpdateScoreText();

                if (destroyedObjectsCount == 6 && SceneManager.GetActiveScene().name == "scene_1")
                {
                    // Activate the CocoCanvas and CocoPopup objects
                    if (cocoCanvasObj != null)
                    {
                        cocoCanvasObj.SetActive(true);
                        DontDestroyOnLoad(cocoCanvasObj);
                        Image popup = cocoPopupObj.GetComponent<Image>();
                        StartCoroutine(ShowCocoPopup(1f, 2f, popup));
                    }
                }

                if (destroyedObjectsCount == 6 && SceneManager.GetActiveScene().name == "scene_2")
                {
                    // Activate the ValentiaPopup object
                    if (ValentiaObj != null)
                    {
                        ValentiaObj.SetActive(true);
                        DontDestroyOnLoad(ValentiaObj);
                        Image popup = ValentiaPopupObj.GetComponent<Image>();
                        StartCoroutine(ShowCocoPopup(1f, 2f, popup));
                    }
                }
            }
        }
    }

    IEnumerator ShowCocoPopup(float delay, float duration, Image popup)
    {
        yield return new WaitForSeconds(delay);

        // Activate the CocoPopup object
        popup.gameObject.SetActive(true);

        float t = 0;
        float startScale = 0.09480993f;
        float endScale = 1.024585f;

        while (t < duration)
        {
            float newScale = Mathf.Lerp(startScale, endScale, t / duration);
            popup.transform.localScale = new Vector3(newScale, newScale, newScale);
            t += Time.deltaTime;
            yield return null;
        }
        popup.transform.localScale = new Vector3(endScale, endScale, endScale);

        // Deactivate the CocoPopup after the scaling animation
        yield return new WaitForSeconds(2f);
        popup.gameObject.SetActive(false);
    }

    void UpdateScoreText()
    {
        if (destroyedObjectsText != null)
        {
            destroyedObjectsText.text = "" + destroyedObjectsCount;
        }
        else
        {
            Debug.LogError(
                "destroyedObjectsText is not assigned. Please assign it in the Unity inspector or make sure the 'Score' GameObject exists and has a Text component."
            );
        }
    }
}