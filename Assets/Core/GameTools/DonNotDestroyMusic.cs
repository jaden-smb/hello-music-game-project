using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DonNotDestroyMusic : MonoBehaviour
{
    private bool isManuallyPaused = false;

    void Start()
    {
        // Ensure the music is playing on start
        if (GetComponent<AudioSource>() != null)
        {
            GetComponent<AudioSource>().Play();
        }
        else
        {
            Debug.LogError("AudioSource is not assigned in the Unity editor");
        }
    }

    private void Awake()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("Music");
        if (objs.Length > 1)
        {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this.gameObject);
    }

    private void Update()
    {
        // Check if the current scene is "main_menu"
        if (SceneManager.GetActiveScene().name == "main_menu")
        {
            // If it is, destroy the music
            Destroy(this.gameObject);
        }

        GameObject CocoPopup = GameObject.FindWithTag("CocoPopup");
        GameObject ValentiaPopup = GameObject.FindWithTag("ValentiaPopup");

        if (SceneManager.GetActiveScene().name == "scene_1" && CocoPopup != null && CocoPopup.activeSelf || SceneManager.GetActiveScene().name == "scene_2" && ValentiaPopup != null && ValentiaPopup.activeSelf)
        {
            if (GetComponent<AudioSource>() != null && GetComponent<AudioSource>().isPlaying)
            {
                GetComponent<AudioSource>().Pause();
            }
        }
        else if (!isManuallyPaused)
        {
            if (GetComponent<AudioSource>() != null && !GetComponent<AudioSource>().isPlaying)
            {
                GetComponent<AudioSource>().Play();
            }
        }

        if (SceneManager.GetActiveScene().name == "scene_2" || SceneManager.GetActiveScene().name == "scene_3")
        {
            GameObject playPauseButtonObject = GameObject.Find("playPauseButton");
            if (playPauseButtonObject != null)
            {
                Button playPauseButton = playPauseButtonObject.GetComponent<Button>();
                if (playPauseButton != null)
                {
                    playPauseButton.onClick.AddListener(PauseMusic);
                }
            }
            GameObject playMusic1 = GameObject.Find("CloseCanvas");
            GameObject playMusic2 = GameObject.Find("CloseCanvas2");
            if (playMusic1 != null && playMusic2 != null)
            {
                Button closeCanvasButton = playMusic1.GetComponent<Button>();
                Button closeCanvasButton_2 = playMusic2.GetComponent<Button>();
                if (closeCanvasButton != null && closeCanvasButton_2 != null)
                {
                    closeCanvasButton.onClick.AddListener(PlayMusic);
                    closeCanvasButton_2.onClick.AddListener(PlayMusic);
                }
            }
        }
    }

    public void PauseMusic()
    {
        if (GetComponent<AudioSource>() != null && GetComponent<AudioSource>().isPlaying)
        {
            GetComponent<AudioSource>().Pause();
            isManuallyPaused = true;
        }

        if (SceneManager.GetActiveScene().name == "scene_2" && GetComponent<AudioSource>() != null && GetComponent<AudioSource>().isPlaying)
        {
            GetComponent<AudioSource>().Pause();
            isManuallyPaused = true;
        }

        if (SceneManager.GetActiveScene().name == "scene_3" && GetComponent<AudioSource>() != null && GetComponent<AudioSource>().isPlaying)
        {
            GetComponent<AudioSource>().Pause();
            isManuallyPaused = true;
        }
    }

    public void PlayMusic()
    {
        if (GetComponent<AudioSource>() != null)
        {
            GetComponent<AudioSource>().UnPause();
            isManuallyPaused = false;
        }

        if (SceneManager.GetActiveScene().name == "scene_2" && GetComponent<AudioSource>() != null && !GetComponent<AudioSource>().isPlaying)
        {
            GetComponent<AudioSource>().UnPause();
            isManuallyPaused = false;
        }

        if (SceneManager.GetActiveScene().name == "scene_3" && GetComponent<AudioSource>() != null && !GetComponent<AudioSource>().isPlaying)
        {
            GetComponent<AudioSource>().UnPause();
            isManuallyPaused = false;
        }
    }
}