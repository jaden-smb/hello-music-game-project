using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Splines;
using UnityEngine.UI;

public class PlayPauseButton : MonoBehaviour
{
    public SplineAnimate splineAnimate;
    public bool isPlaying = true;
    public Button ButtonPlayPause;
    public Button CloseCanvasButton;
    public Button CloseCanvasButton_2;
    public Sprite playSprite;
    public Sprite pauseSprite;
    public Canvas pauseCanvas;

    private void Start()
    {
        splineAnimate = FindObjectOfType<SplineAnimate>();
        // we have many splinesanimations in the scene_3
        // So, we need only one splineanimation of the main character
        if (SceneManager.GetActiveScene().name == "scene_3")
        {
            splineAnimate = GameObject.Find("Cohete").GetComponent<SplineAnimate>();
            Debug.Log("splineAnimate in scene_3: " + splineAnimate);
        }

        if (splineAnimate != null)
        {
            splineAnimate.Play();
        }
        else
        {
            Debug.LogError("splineAnimate is not assigned in the Unity editor");
        }

        ButtonPlayPause.onClick.AddListener(TogglePlayPause);
        CloseCanvasButton.onClick.AddListener(CloseCanvas);
        CloseCanvasButton_2.onClick.AddListener(CloseCanvas);
        pauseCanvas.gameObject.SetActive(false);
    }


    private void Update()
    {
        if (SceneManager.GetActiveScene().name == "scene_1" && splineAnimate != null && !splineAnimate.IsPlaying && splineAnimate.ElapsedTime >= splineAnimate.Duration && !pauseCanvas.gameObject.activeSelf)
        {
            SceneManager.LoadScene("scene_2");
        }
        if (SceneManager.GetActiveScene().name == "scene_2" && splineAnimate != null && !splineAnimate.IsPlaying && splineAnimate.ElapsedTime >= splineAnimate.Duration && !pauseCanvas.gameObject.activeSelf)
        {
            SceneManager.LoadScene("scene_3");
        }
        if (SceneManager.GetActiveScene().name == "scene_3" && splineAnimate != null && !splineAnimate.IsPlaying && splineAnimate.ElapsedTime >= splineAnimate.Duration && !pauseCanvas.gameObject.activeSelf)
        {
            SceneManager.LoadScene("main_menu");
        }
    }
    public void TogglePlayPause()
    {
        if (isPlaying)
        {
            PauseSplineAnimation();
            ButtonPlayPause.gameObject.SetActive(false);
            pauseCanvas.gameObject.SetActive(true);
        }
        else
        {
            PlaySplineAnimation();
            ButtonPlayPause.gameObject.SetActive(true);
            pauseCanvas.gameObject.SetActive(false);
        }
    }

    public void CloseCanvas()
    {
        pauseCanvas.gameObject.SetActive(false);
        PlaySplineAnimation();
        ButtonPlayPause.gameObject.SetActive(true);
    }

    public void PauseSplineAnimation()
    {
        if (splineAnimate != null)
        {
            splineAnimate.Pause();
            isPlaying = false;
        }
        else
        {
            Debug.LogError("splineAnimate is not assigned in the Unity editor");
        }
    }

    public void PlaySplineAnimation()
    {
        if (splineAnimate != null)
        {
            splineAnimate.Play();
            isPlaying = true;
        }
        else
        {
            Debug.LogError("splineAnimate is not assigned in the Unity editor");
        }
    }
}