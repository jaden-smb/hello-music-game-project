using UnityEngine;
using UnityEngine.UI;

public class AudioButtonController : MonoBehaviour
{
    public AudioSource backgroundMusic;

    void Awake()
    {
        GameObject musicObject = GameObject.FindGameObjectWithTag("Music");
        if (musicObject != null)
        {
            backgroundMusic = musicObject.GetComponent<AudioSource>();
        }
    }

    public void ToggleBackgroundMusic()
    {
        if (backgroundMusic != null)
        {
            if (backgroundMusic.isPlaying)
            {
                backgroundMusic.Pause();
            }
            else
            {
                backgroundMusic.Play();
            }
        }
        else
        {
            Debug.LogError("Background music source not found");
        }
    }

    public void PauseMusic()
    {
        if (backgroundMusic != null && backgroundMusic.isPlaying)
        {
            backgroundMusic.Pause();
        }
    }

    void Start()
    {
        GetComponent<Button>().onClick.AddListener(ToggleBackgroundMusic);
    }

    void Update()
    {
        GameObject CocoPopup = GameObject.FindWithTag("CocoPopup");
        GameObject ValentiaPopup = GameObject.FindWithTag("ValentiaPopup");
        // If CocoPopup is active, pause the music
        if (CocoPopup != null && CocoPopup.activeSelf || ValentiaPopup != null && ValentiaPopup.activeSelf)
        {
            if (backgroundMusic != null && backgroundMusic.isPlaying)
            {
                backgroundMusic.Pause();
            }
        }
        else
        {
            if (backgroundMusic != null && !backgroundMusic.isPlaying)
            {
                backgroundMusic.Play();
            }
        }
    }
}