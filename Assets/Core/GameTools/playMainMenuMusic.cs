using UnityEngine;

public class playMainMenuMusic : MonoBehaviour 
{
    public AudioSource audioSource; // Drag your AudioSource here in the inspector

    // Use this for initialization
    void Start() 
    {
        audioSource.Play();
    }
}