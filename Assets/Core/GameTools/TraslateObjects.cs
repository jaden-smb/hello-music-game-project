using System.Collections;
using UnityEngine;

public class ScaleObjects : MonoBehaviour 
{
    public float delay = 4.0f;
    public float transitionDuration = 2.0f;
    private float startScale = 1f;
    private float endScale = 0.15847f;

    void Start()
    {
        StartCoroutine(ScaleObjective());
    }

    IEnumerator ScaleObjective()
    {
        // Wait for the specified delay
        yield return new WaitForSeconds(delay);

        float t = 0;
        GameObject objective = GameObject.Find("Objective");
        GameObject pauseCanvas = GameObject.Find("PauseCanvas");
        if (objective != null)
        {
            while (t < transitionDuration)
            {
                // If PauseCanvas is active, skip this frame
                if (pauseCanvas != null && pauseCanvas.activeInHierarchy)
                {
                    yield return null;
                    continue;
                }

                float newScale = Mathf.Lerp(startScale, endScale, t / transitionDuration);
                objective.transform.localScale = new Vector3(newScale, newScale, newScale);
                t += Time.deltaTime;
                yield return null;
            }
            // Ensure the final scale is set correctly
            objective.transform.localScale = new Vector3(endScale, endScale, endScale);
            // Make the object disappear
            objective.SetActive(false);
        }
    }
}